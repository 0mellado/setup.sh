usr=$(whoami)
sudo pacman -Sy fish
# crear usuario
sudo useradd -m estudiante
sudo passwd -d estudiante
sudo chsh -s /bin/fish estudiante
sudo chsh -s /bin/fish 
sudo chsh -s /bin/fish admin
# Instalando los programas
echo -e "\n[*] Instalando los programas"

sudo pacman -Syu
sudo pacman -Sy git make mariadb mysql apache php php-apache php-fpm jdk-openjdk firefox python2 python gimp nginx

echo -e "\n[*] Iniciando los servicios"
sudo systemctl restart httpd
sudo mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
sudo systemctl enable mysqld
sudo systemctl start mysqld.service
sudo mysql_secure_installation

echo -e "\n[*] Instalando yay:"
echo -e "\n[*] Cambiando de directorio a /opt"
cd /opt
echo -e "\n[*] Clonando el repositorio:"
sudo git clone https://aur.archlinux.org/yay-git.git
echo -e "\n[*] Cambiando de propietario el directorio yay-git"
sudo chown -R $usr:$usr ./yay-git
echo -e "\n[*] Cambiando de directorio a ./yay-git"
cd yay-git/
echo -e "\n[*] Compilando:"
makepkg -si
yay -S vscodium-bin figma-linux brave-bin xmind pseint-bin libreoffice-still-es 

